import React, { useEffect } from 'react';
import * as Notifications from 'expo-notifications';

function  notifee () {
    useEffect(() => {
        const requestNotificationPermission = async () => {
          try {
            const { status } = await Notifications.getPermissionsAsync();
            if (status !== 'granted') {
              const { status: newStatus } = await Notifications.requestPermissionsAsync();
              if (newStatus !== 'granted') {
                console.log('Bildirim izni reddedildi.');
              }
            }
          } catch (error) {
            console.error('Bildirim izni hatası:', error);
          }
        };
      
        requestNotificationPermission();
      }, []);
}
